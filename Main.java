public class Main
{
    public static void main(String[] args)
    {
        Food[] allTheFood= new Food[5];
        allTheFood[0] = new Wings();
        allTheFood[0].setDiscription("Wings");
        allTheFood[1] = new Food();
        allTheFood[1].setDiscription("Buttery Popcorn");
        ((Wings)allTheFood[0]).setRating(20);
        
        System.out.println(allTheFood[0].toString());
        System.out.println(allTheFood[1].toString());
        System.out.println("Let's have a party. People better bring a lot of food.");
    }
}