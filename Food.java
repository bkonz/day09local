public class Food
{
    private String description;
    private int calories;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    /**
     * set the discription
     * @praram indiscription the new discription
     */
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    public int getCalories() {
        return calories;
    }
    
    public void setCalories(int calories) {
        this.calories = calories;
    }
    
    @Override
    public String toString() {
        String s = "Somebody brought " + getDiscription();
        return s;
}